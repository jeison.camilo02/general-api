<?php

namespace Inmovsoftware\GeneralApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\GeneralApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\GeneralApi\Models\V1\Logs;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use DB;
use Log;

class InmovTechGeneralServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');



        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'generalLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'generalLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'general');

        DB::listen(function($query) {
            Log::info(
                $query->sql,
                $query->bindings,
                $query->time
            );
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\GeneralApi\Models\V1\Logs');
        $this->app->make('Inmovsoftware\GeneralApi\Http\Controllers\V1\GeneralController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
