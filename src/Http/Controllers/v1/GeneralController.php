<?php
namespace Inmovsoftware\GeneralApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\GeneralApi\Models\V1\Logs;
use Inmovsoftware\GeneralApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use SendGrid\Mail\Mail as Mail;
use SendGrid as SendGrid;

class GeneralController extends Controller
{
    public function index(Request $request)
    {

    }


    public function store(Request $request)
    {

    }

    public function app_store(Request $request)
    {

    }


    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {

    }


}
